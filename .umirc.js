import os from 'os';
import routesConfig from './config/routeconfig';
// ref: https://umijs.org/config/

export default {
  treeShaking: true,
  routes: routesConfig,
  history: 'browser',
  cssLoaderOptions: {
    localIdentName: '[local]',
  },

  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    [
      'umi-plugin-react',
       {
      
      dva: {
        immer: true,
      },
      antd: true,
      dynamicImport: true,
      title: 'umi-demo',
      ...(!process.env.TEST && os.platform() === 'darwin'
        ? {
          dll: {

            include: ['dva', 'dva/router', 'dva/saga', 'dva/fetch'],
            exclude: ['@babel/runtime'],
          },
          hardSource: false,
        }
        : {}),
        
      locale: {
        enable: true,
        default: 'en-US',
      },

      routes: {

        exclude: [
          /models\//,
          /services\//,
          /model\.(t|j)sx?$/,
          /service\.(t|j)sx?$/,
          /components\//,
        ],
      },
    }],
  ],
}
