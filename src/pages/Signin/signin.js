import React, { useState } from 'react';
import './signin.scss';
import { connect } from 'dva';
import { NavLink } from 'react-router-dom';
import 'antd/dist/antd.css';
import { Form, Input, Button, Row, Col } from 'antd';
import signinimg from '../../assets/Signin.png';

function signin({ dispatch }) {
  const onfinishFailedData = errorInfo => {
    console.log('Failed:', errorInfo);
  };
  const onfinishData = value => {
    dispatch({
      type: 'userdetails/Login',
      payload: { value },
    });
  };

  return (
    <>
      <div className="main">
        <Row>
          <div className="Card">
            <img src={signinimg} alt="login" className="imgfluid" />
            <Col xs={22} sm={22} md={22} xl={10}>
              <Form
                name="basic"
                id="resetForm"
                onFinishFailed={onfinishFailedData}
                onFinish={onfinishData}
                labelCol={{
                  span: 24,
                }}
                wrapperCol={{
                  span: 24,
                }}
                initialValues={{
                  remember: true,
                }}
                autoComplete="off"
                className="formL"
              >
                <div className="h2">Sign in</div>
                <Form.Item
                  label="E-mail"
                  name="email"
                  rules={[
                    {
                      type: 'email',
                      message: 'The input is not valid E-mail!',
                    },
                    {
                      required: true,
                      message: 'Please Enter Your E-mail!',
                    },
                  ]}
                >
                  <Input placeholder="Enter Email" />
                </Form.Item>

                <Form.Item
                  label="Password"
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: 'Please Enter Your Password!',
                    },
                    {
                      min: 8,
                      message: 'Password Must Be Minimum 8 Characters.',
                    },
                  ]}
                >
                  <Input.Password placeholder="Enter Password" />
                </Form.Item>
                <Form.Item
                  name="reg"
                  wrapperCol={{
                    offset: 4,
                    span: 20,
                  }}
                >
                  <NavLink to="/signup">Don't have account ? sign up</NavLink>
                </Form.Item>

                <Form.Item
                  wrapperCol={{
                    offset: 4,
                    span: 20,
                  }}
                >
                  <Button shape="round" htmlType="submit" className="btn">
                    Login
                  </Button>
                </Form.Item>
              </Form>
            </Col>
          </div>
        </Row>
      </div>
    </>
  );
}

export default connect()(signin);
