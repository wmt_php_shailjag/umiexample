import './about.scss';
import React from 'react';
import 'antd/dist/antd.css';
import { Row, Col, Card, Rate, Statistic } from 'antd';
import AboutMain from '../../assets/AboutMain.gif';
import UmiImg from '../../assets/UmiImg.png';
import User1 from '../../assets/AboutUser1.jpeg';
import User2 from '../../assets/AboutUser2.jpg';
import User3 from '../../assets/AboutUser3.jpeg';
import { IntroductionMain, Contact } from '../../../const/const';

function About() {
  const { Meta } = Card;

  return (
    <>
      <div className="mainContact">
        {/*Form and img*/}
        <div class="top">
          <Row>
            <Col xs={{ span: 6, offset: 2 }} md={{ span: 5 }} lg={{ span: 9 }}>
              <img src={AboutMain} alt="gif" className="imgfluidAbout" />
            </Col>
            <Col
              xs={{ span: 22, offset: 1 }}
              sm={16}
              md={{ span: 22 }}
              lg={{ span: 10 }}
              className="md"
            >
              <div className="h2At">About Us</div>
              {IntroductionMain.map(Intro => (
                <div className="para"> {Intro.description}</div>
              ))}
            </Col>
          </Row>
        </div>

        {/*help card*/}
        <div className="mainContact">
          <Row>
            <Col
              xs={{ span: 22, offset: 2 }}
              sm={{ span: 22, offset: 2 }}
              md={{ span: 19, offset: 5 }}
              xl={20}
            >
              <div className="crdContact">
                <Card hoverable className="CardTop">
                  <Meta
                    title="Pluggable"
                    description=" The entire lifecycle of umi is composed of plugins. Features such as pwa, on-demand loading, one-click switching preact, one-button compatibility ie9, etc., are all implemented by plugins."
                  />
                </Card>
                <Card hoverable className="CardTop">
                  <Meta
                    title="Out Of Box"
                    description="You only need an umi dependency to start development without having to install react, preact, webpack, react-router, babel, jest, and more."
                  />
                </Card>
                <Card hoverable className="CardTop">
                  <Meta
                    title="Conventional Routing"
                    description="Next.js like and full featured routing conventions, support permissions, dynamic routing, nested routing, and more."
                  />
                </Card>
              </div>
            </Col>
          </Row>
        </div>
      </div>
      <div className="strip">
        <Row>
          <Col
            xs={{ span: 22, offset: 4 }}
            sm={{ span: 22, offset: 2 }}
            md={{ span: 4, offset: 3 }}
          >
            <Statistic title="GITHUB STARS" value="71.4k" className="white" />
          </Col>
          <Col
            xs={{ span: 22, offset: 4 }}
            sm={{ span: 22, offset: 2 }}
            md={{ span: 4, offset: 4 }}
          >
            <Statistic title="WEEKLY NPM DOWNLOADS" value="610k (May 2021)" />
          </Col>
          <Col
            xs={{ span: 22, offset: 4 }}
            sm={{ span: 22, offset: 2 }}
            md={{ span: 4, offset: 4 }}
          >
            <Statistic title="STACK OVERFLOW QUESTIONS" value="3.2k" />
          </Col>
        </Row>
      </div>

      <div className="mainContact">
        <div>
          <Col offset={2} className="paraAbouth">
            Umi js (initial page)
          </Col>
        </div>
        <div className="paraAbout">
          <Col span={18} offset={2}>
            umi is a routing-based framework that supports next.js-like conventional routing and
            various advanced routing functions, such as routing-level on-demand loading. umi is a
            routing-based framework that supports next.js-like conventional routing and various
            advanced routing functions, such as routing-level on-demand loading.
          </Col>
        </div>
        <div>
          <Col offset={2}>
            <img src={UmiImg} className="umi"></img>
          </Col>
        </div>
      </div>

      <div className="slider">
        <div className="p">Testimonials</div>
        <div className="crdContact test">
          <div className="pb9">
            <Row>
              <Col xs={{ span: 22, offset: 1 }} md={{ span: 8 }}>
                <Card cover={<img src={User1} className="userimg" />} className="Cardslider">
                  <Meta
                    title="
                  Branden Metcalfe"
                    description="A very capable design system, especially for Enterprise apps. Includes every imaginable
                   component you could ask for. Design is minimalistic and clean, as you'd want an enterprise app to be. Since it doesn't have 
                   some of the eye candy offered by Material UI, it's also faster and less janky. Documentation is pretty decent, and the community 
                   around Ant very large (although some of it is inChinese which I unfortunately do not speak). Overall great experience."
                  />
                  <Rate disabled defaultValue={5} />
                </Card>
              </Col>
            </Row>
          </div>
          <div className="pb9">
            <Row>
              <Col xs={{ span: 22, offset: 1 }} md={{ span: 8 }}>
                <Card cover={<img src={User2} className="userimg" />} className="Cardslider">
                  <Meta
                    title="
                  Branden Metcalfe"
                    description="A very capable design system, especially for Enterprise apps. Includes every imaginable
                   component you could ask for. Design is minimalistic and clean, as you'd want an enterprise app to be. Since it doesn't have 
                   some of the eye candy offered by Material UI, it's also faster and less janky. Documentation is pretty decent, and the community 
                   around Ant very large (although some of it is inChinese which I unfortunately do not speak). Overall great experience."
                  />
                  <Rate disabled defaultValue={4} />
                </Card>
              </Col>
            </Row>
          </div>
          <div className="pb9">
            <Row>
              <Col xs={{ span: 22, offset: 1 }} md={{ span: 8 }}>
                <Card cover={<img src={User3} className="userimg" />} className="Cardslider">
                  <Meta
                    title="
                  Branden Metcalfe"
                    description="A very capable design system, especially for Enterprise apps. Includes every imaginable
                   component you could ask for. Design is minimalistic and clean, as you'd want an enterprise app to be. Since it doesn't have 
                   some of the eye candy offered by Material UI, it's also faster and less janky. Documentation is pretty decent, and the community 
                   around Ant very large (although some of it is inChinese which I unfortunately do not speak). Overall great experience."
                  />
                  <Rate disabled defaultValue={5} />
                </Card>
              </Col>
            </Row>
          </div>
        </div>
      </div>
      {/*help card*/}
      <div className="mapbottom">
        {/* */}
        <div>
          <div className="p">Let's Get Touch In Another Way</div>
        </div>
        <Row>
          <Col xs={{ span: 22 }} sm={{ span: 22, offset: 2 }} md={{ span: 20, offset: 2 }} xl={20}>
            <div className="crdContact side">
              {Contact.map(Contacts => (
                <Card hoverable cover={Contacts.icon} className="Cardsider">
                  <Meta title={Contacts.title} description={Contacts.description} />
                </Card>
              ))}
            </div>
          </Col>
        </Row>
      </div>
    </>
  );
}
export default About;
