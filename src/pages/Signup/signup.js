import React from 'react';
import './signup.scss';
import { NavLink } from 'react-router-dom';
import 'antd/dist/antd.css';
import { Form, Input, Button, Radio, Row, Col } from 'antd';
import { connect } from 'dva';
import signupimg from '../../assets/Signup.jpg';

function signup({ dispatch }) {
  const onFinishFailedData = errorInfo => {
    console.log('Failed:', errorInfo);
  };
  const onFinishData = value => {
    dispatch({
      type: 'userdetails/Register',
      payload: { value },
    });
  };

  return (
    <>
      <div className="main">
        <Row>
          <div className="CardR">
            <img src={signupimg} alt="register" className="imgfluidR" />

            <Col xs={22} sm={22} md={22} xl={10}>
              <Form
                name="basic"
                id="form"
                labelCol={{
                  span: 24,
                }}
                wrapperCol={{
                  span: 22,
                }}
                onFinish={onFinishData}
                onFinishFailed={onFinishFailedData}
                className="formreg"
              >
                <Form.Item
                  wrapperCol={{
                    offset: 7,
                    span: 15,
                  }}
                >
                  <div className="h2R">Sign up</div>
                </Form.Item>
                <Form.Item
                  label="Name"
                  name="Name"
                  className="inpt"
                  rules={[
                    {
                      required: true,
                      message: 'Please Enter your Name!',
                    },
                    {
                      min: 5,
                      message: 'Username must be minimum 5 characters.',
                    },
                  ]}
                >
                  <Input placeholder="Enter Username" />
                </Form.Item>
                <Form.Item
                  name="email"
                  label="E-mail"
                  className="inpt"
                  rules={[
                    {
                      type: 'email',
                      message: 'The input is not valid E-mail!',
                    },
                    {
                      required: true,
                      message: 'Please Entre your E-mail!',
                    },
                  ]}
                >
                  <Input placeholder="Enter Email" />
                </Form.Item>
                <Form.Item
                  label="Password"
                  name="password"
                  className="inpt"
                  rules={[
                    {
                      required: true,
                      message: 'Please Set password!',
                    },
                    {
                      min: 8,
                      message: 'Password must be minimum 8 characters.',
                    },
                    {
                      max: 16,
                      message: 'Password must be max upto 16 characters.',
                    },
                  ]}
                >
                  <Input.Password placeholder="Enter Password" />
                </Form.Item>

                <Form.Item
                  label="Gender"
                  name="Gender"
                  rules={[
                    {
                      required: true,
                      message: 'Please Select Gender!',
                    },
                  ]}
                >
                  <Radio.Group name="radiogroup">
                    <Radio value="Male" checked>
                      Male
                    </Radio>
                    <Radio value="Female">Female</Radio>
                    <Radio value="Other">Other</Radio>
                  </Radio.Group>
                </Form.Item>

                <Form.Item
                  name="hveac"
                  wrapperCol={{
                    offset: 4,
                    span: 20,
                  }}
                >
                  <NavLink to="/signin">Already have account? sign in</NavLink>
                </Form.Item>
                <Form.Item
                  wrapperCol={{
                    offset: 4,
                    span: 20,
                  }}
                >
                  <Button shape="round" htmlType="submit" className="btn">
                    Register
                  </Button>
                </Form.Item>
              </Form>
            </Col>
          </div>
        </Row>
      </div>
    </>
  );
}

export default connect()(signup);
