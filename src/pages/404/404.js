import React from "react";
import './page404.scss';
import { NavLink } from 'react-router-dom'

function page404() {
    return(
        <>
        <div className="container">
        <NavLink to="/Home">
            <img src="https://cdn.dribbble.com/users/469578/screenshots/2597126/404-drib23.gif" className="imgfluidContact"></img>
        </NavLink>
        </div>
        </>
    );
}

export default page404