import './contact.scss';
import React from 'react';
import 'antd/dist/antd.css';
import { connect } from 'dva';
import { Form, Input, Button, Row, Col, Card } from 'antd';
import ContactMain from '../../assets/ContactMain.gif';
import { Contact, SupportFeature } from '../../../const/const';

function contact({ dispatch }) {
  const { TextArea } = Input;
  const { Meta } = Card;

  const onfinishData = value => {
    dispatch({
      type: 'userdetails/Contact',
      payload: { value },
    });
  };

  const onfinishFailedData = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <>
      <div className="mainContact">
        {/*Form and img*/}
        <div class="topa">
          <Row>
            <img src={ContactMain} alt="image" className="imgfluidContact" />
            <Col xs={22} sm={16} md={16} lg={20} xl={10}>
              <Form
                name="basic"
                id="resetForm"
                labelCol={{
                  span: 24,
                }}
                wrapperCol={{
                  span: 24,
                }}
                initialValues={{
                  remember: true,
                }}
                onFinishFailed={onfinishFailedData}
                onFinish={onfinishData}
                autoComplete="off"
                className="FormContact"
              >
                <div className="h2">Get In Touch With US</div>
                <Form.Item
                  label="Name"
                  name="Name"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter your name!',
                    },
                    {
                      min: 5,
                      message: 'Name must be minimum 5 characters.',
                    },
                  ]}
                >
                  <Input placeholder="Enter Name" />
                </Form.Item>

                <Form.Item
                  name="email"
                  label="E-mail"
                  rules={[
                    {
                      type: 'email',
                      message: 'The input is not valid E-mail!',
                    },
                    {
                      required: true,
                      message: 'Please Entre your E-mail!',
                    },
                  ]}
                >
                  <Input placeholder="Enter E-mail" />
                </Form.Item>
                <Form.Item
                  name="Description"
                  label="Description"
                  rules={[
                    {
                      required: true,
                      message: 'Please Enter Description!',
                    },
                    {
                      min: 5,
                      message: 'Add detailed description please',
                    },
                  ]}
                >
                  <TextArea placeholder="Description" rows={4} />
                </Form.Item>

                <Form.Item
                  wrapperCol={{
                    offset: 5,
                    span: 21,
                  }}
                >
                  <Button shape="round" htmlType="submit" className="btnsubmit">
                    Submit
                  </Button>
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </div>

        {/*help card*/}
        <Row>
          <Col xs={{ span: 22 }} sm={{ span: 22, offset: 2 }} md={{ span: 20, offset: 3 }} xl={20}>
            <div className="crdContact">
              {SupportFeature.map(SupportFeatures => (
                <Card hoverable cover={SupportFeatures.icon} className="CardTopa">
                  <Meta title={SupportFeatures.title} description={SupportFeatures.description} />
                </Card>
              ))}
            </div>
          </Col>
        </Row>
      </div>
      {/*strip*/}
      <Row>
        <Col>
          <div className="strip">
            <div className="row">
              <div className="text">See more of our works</div>
              <div className="btnright">
                <Button primary htmlType="submit">
                  Explore Our Work
                </Button>
              </div>
            </div>
          </div>
        </Col>
      </Row>

      <div className="mapbottom">
        {/* */}
        <div>
          <div className="p">Let's Get Touch In Another Way</div>
        </div>
        <Row>
          <Col xs={{ span: 22 }} sm={{ span: 22, offset: 2 }} md={{ span: 20, offset: 2 }} xl={20}>
            <div className="crdContact side">
              {Contact.map(Contacts => (
                <Card hoverable cover={Contacts.icon} className="Cardsider">
                  <Meta title={Contacts.title} description={Contacts.description} />
                </Card>
              ))}
            </div>
          </Col>
        </Row>
      </div>
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d198740.83318920605!2d-77.15466070729937!3d38.89367081299196!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b7c6de5af6e45b%3A0xd6e28ec00254a198!2sWashington%2C%20District%20of%20Columbia%2C%20USA!5e0!3m2!1sen!2sin!4v1632137717076!5m2!1sen!2sin"
        allowfullscreen=""
        loading="lazy"
        className="Map"
      />
    </>
  );
}

export default connect()(contact);
