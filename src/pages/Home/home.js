import './home.scss';
import React from 'react';
import 'antd/dist/antd.css';
import { Row, Col, Card, Statistic } from 'antd';
import HomeBg from '../../assets/HomeBg.jpg';
import HomeMain from '../../assets/HomeMain.gif';
import { Feature, Introduction, IntroductionMain, Brand, HomeFeature } from '../../../const/const';

function Home() {
  const { Meta } = Card;

  return (
    <>
      <div>
        <Row>
          <Col span={24}>
            <div class="container">
              <img src={HomeBg} alt="Notebook" style={{ width: '100%' }} className="imghome" />
              <div className="content">
                <div className="sAbout">
                  <h1>UmiJS</h1>
                  {Introduction.map(Intro => (
                    <p> {Intro.description}</p>
                  ))}
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div>

      <div class="top">
        <Row>
          <Col xs={{ span: 6, offset: 2 }} md={{ span: 5 }} lg={{ span: 9 }}>
            <img src={HomeMain} alt="gif" className="imgfluidAbouta" />
          </Col>
          <Col
            xs={{ span: 22, offset: 1 }}
            sm={16}
            md={{ span: 22 }}
            lg={{ span: 10 }}
            className="md"
          >
            <div className="h2At">Indroduction</div>
            {IntroductionMain.map(Intro => (
              <div className="para"> {Intro.description}</div>
            ))}
          </Col>
        </Row>
      </div>

      <div className="mainContact">
        <div class="top">
          <Row>
            <Col
              xs={{ span: 22, offset: 1 }}
              sm={16}
              md={{ span: 22 }}
              lg={{ span: 10, offset: 8 }}
              className="md"
            >
              <Card hoverable className="card_feature">
                <div className="h2At">Features</div>
                {Feature.map(Features => (
                  <div className="space"> {Features.description}</div>
                ))}
              </Card>
            </Col>
          </Row>
        </div>
      </div>

      <div className="mainContact">
        <Row>
          <Col xs={{ span: 22 }} sm={{ span: 22, offset: 2 }} md={{ span: 20, offset: 2 }} xl={20}>
            <div className="crdContact">
              {HomeFeature.map(Home => (
                <Card cover={Home.icon} className="CardTopa">
                  <Meta title={Home.title} description={Home.description} />
                </Card>
              ))}
            </div>
          </Col>
        </Row>
      </div>

      <div className="mainContact">
        <div class="top">
          <div className="h2At">Architecture</div>
          <img
            alt="example"
            src="https://gw.alipayobjects.com/zos/rmsportal/zvfEXesXdgTzWYZCuHLe.png"
            className="img_arc"
          />
        </div>
      </div>

      <div className="strip">
        <Row>
          <Col
            xs={{ span: 22, offset: 4 }}
            sm={{ span: 22, offset: 2 }}
            md={{ span: 4, offset: 3 }}
          >
            <Statistic title="GITHUB STARS" value="71.4k" className="white" />
          </Col>
          <Col
            xs={{ span: 22, offset: 4 }}
            sm={{ span: 22, offset: 2 }}
            md={{ span: 4, offset: 4 }}
          >
            <Statistic title="WEEKLY NPM DOWNLOADS" value="610k (May 2021)" />
          </Col>
          <Col
            xs={{ span: 22, offset: 4 }}
            sm={{ span: 22, offset: 2 }}
            md={{ span: 4, offset: 4 }}
          >
            <Statistic title="STACK OVERFLOW QUESTIONS" value="3.2k" />
          </Col>
        </Row>
      </div>

      <div className="mainContact">
        <div className="h2At">Brand That Matters</div>

        <div className="crdContactH ">
          {Brand.map(brand => (
            <div className="imgSpace">
              <Card className="crdview" cover={brand.icon}></Card>
            </div>
          ))}
          {Brand.map(brand => (
            <div className="imgSpace">
              <Card className="crdview" cover={brand.icon}></Card>
            </div>
          ))}
          {Brand.map(brand => (
            <div className="imgSpace">
              <Card className="crdview" cover={brand.icon}></Card>
            </div>
          ))}
          {Brand.map(brand => (
            <div className="imgSpace">
              <Card className="crdview" cover={brand.icon}></Card>
            </div>
          ))}
          {Brand.map(brand => (
            <div className="imgSpace">
              <Card className="crdview" cover={brand.icon}></Card>
            </div>
          ))}
          {Brand.map(brand => (
            <div className="imgSpace">
              <Card className="crdview" cover={brand.icon}></Card>
            </div>
          ))}
          {Brand.map(brand => (
            <div className="imgSpace">
              <Card className="crdview" cover={brand.icon}></Card>
            </div>
          ))}
          {Brand.map(brand => (
            <div className="imgSpace">
              <Card className="crdview" cover={brand.icon}></Card>
            </div>
          ))}
          {Brand.map(brand => (
            <div className="imgSpace">
              <Card className="crdview" cover={brand.icon}></Card>
            </div>
          ))}
          {Brand.map(brand => (
            <div className="imgSpace">
              <Card className="crdview" cover={brand.icon}></Card>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}

export default Home;
