import React from 'react';
import styles from'./Footer.scss';
import { Link } from 'umi';

function Footer() {
    return (

        <div>
            <footer className={styles.footerdistributed}>
                <div className={styles.footerleft}>
                    <h3>
                        Design <span>umi js</span>
                    </h3>

                    <p className={styles.footerlinks}>
                       
                        <Link to="/Home">Home</Link>|
                            <Link to="/About">About</Link>|
                        
                            <Link to="/Contact">Contact</Link>
                        
                    </p>

                    <p className={styles.footercompanyname}>© 2021 Demo Pvt. Ltd.</p>
                </div>

                <div className={styles.footercenter}>
                    <div className={styles.txt}>

                        <p>
                             Washington, D.C., USA
                        </p>
                    </div>

                    <div className={styles.txt}>

                        <p> +91 12345678912</p>
                    </div>
                    <div className={styles.txt}>


                        <p>
                            <a>umi@gmail.com</a>
                        </p>
                    </div>
                </div>
                <div className={styles.footerright}>
                    <p className={styles.footercompanyabout}>
                        <span>About the company</span>
                        Just like action speaks louder than words, our work is the best way
                        to get to know about us. The name itself suggests we're proficient
                        in Web and Mobile App development tailed with providing best IT
                        solution for various sectors.
                    </p>
                </div>
            </footer>
        </div>

    );
}
export default Footer