
import React from 'react';
import style from './Header.scss';
import { Layout, Menu, } from "antd";
import "antd/dist/antd.css";
import { Link } from 'umi';

function Header() {
    const { Header } = Layout;
    return (

        <div>
            <Layout >
                <Header>
                    <div className={style.logo}></div>
                    <div className={style.layout} >
                        <Menu theme="dark" mode="horizontal">
                            
                            <Menu.Item>
                                <Link to="/home">
                                    Home
                                </Link>
                            </Menu.Item>
                            <Menu.Item >
                                <Link to="/about">
                                    About us
                                </Link>
                            </Menu.Item>
                            <Menu.Item>
                                <Link to="/contact">
                                    Contact us
                                </Link>
                            </Menu.Item>
                            <Menu.Item >
                                <Link to="/signin">
                                Sign in
                                </Link>
                            </Menu.Item>
                            <Menu.Item >
                                <Link to="/signup">
                                Sign up
                                </Link>
                            </Menu.Item>
                        </Menu>
                    </div>
                </Header>
            </Layout>
        </div>
    );
}
export default Header