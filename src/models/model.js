
import * as usersService from '../services/userServices';
var data = [];
export default {
    namespace: 'userdetails',
    initialstate: [],
    reducers: {
        'Register'(payload) {
            return [...data, payload];
        },
        'Login'(payload) {
            return payload;
        },
        'Contact'(payload) {
            return payload;
        },
    },
    effects: {
        *fetch({ call, put }) {
            yield call(usersService.fetch);
            yield put({ type: 'fetch' });
          },
        *Register({ payload: values }, { call, put }) {
            yield call(usersService.Register, values);
            yield put({ type: 'fetch' });
        },
        *Login({ payload: values }, { call, put }) {
            yield call(usersService.Login, values);
            yield put({ type: 'fetch' });
        },
        *Contact({ payload: values }, { call, put }) {
            yield call(usersService.Contact, values);
            yield put({ type: 'fetch' });
        },
    },
};
