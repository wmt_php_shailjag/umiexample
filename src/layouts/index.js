import './index.scss';
import React from 'react';
import Header from '../component/Header/Header';
import Footer from '../component/Footer/Footer';


function BasicLayout(props) {
  return (
  <div>
    <Header />
      {props.children}
      <Footer />
      </div>
  );
}

export default BasicLayout;
