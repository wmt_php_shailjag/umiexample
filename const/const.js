import {
  CrownTwoTone,
  DollarTwoTone,
  ShopTwoTone,
  BoxPlotTwoTone,
  ApiTwoTone,
  EnvironmentTwoTone,
  PhoneTwoTone,
  MailTwoTone,
} from '@ant-design/icons';

import ContactMain from '../src/assets/ContactMain.gif';
import Support from '../src/assets/Support.jpg';
import RequestDemo from '../src/assets/RequestDemo.png';
import Inquiry from '../src/assets/Inquiry.png';
import Help from '../src/assets/Help.png';

export const Introduction = [
  {
    description:
      'umi is a routing-based framework that supports next.js-like conventional routing and various advanced routing functions, such as routing-level on-demand loading. umi is a routing-based framework that supports next.js-like conventional routing and various advanced routing functions, such as routing-level on-demand loading umi is a routing-based framework that supports next.js-like conventional routing and various advanced routing functions, such as routing-level on-demand loading',
  },
];

export const IntroductionMain = [
  {
    description:
      'umi is a routing-based framework that supports next.js-like conventional routing and various advanced routing functions, such as routing-level on-demand loading. umi is a routing-based framework that supports next.js-like conventional routing and various advanced routing functions, such as routing-level on-demand loading umi is a routing-based framework that supports next.js-like conventional routing and various advanced routing functions, such as routing-level on-demand loading',
  },
  {
    description:
      ' umi is the basic front-end framework of Ant Financial, and has served 600+ applications directly or indirectly, including Java,node, mobile apps, Hybrid apps, pure front-end assets apps, CMS apps, and more. umi has served our internal users very well and we hope that it can also serve external users well.',
  },
  {
    description:
      '   Design is minimalistic and clean, as you d want an enterprise app to be. Since it doesnt have some of the eye candy offered  by Material UI',
  },
  {
    description:
      '  umi is the basic front-end framework of Ant Financial, and has served 600+ applications directly or indirectly, including Java, node, mobile apps, Hybrid apps, pure front-end assets apps, CMS apps, and more. umi has served our internal users very well and we hope that it can also  serve external users well.',
  },
];

export const Brand = [
  {
    icon: <CrownTwoTone className="imgbrand" twoToneColor="#000" />,
  },
  {
    icon: <DollarTwoTone className="imgbrand" twoToneColor="#000" />,
  },
  {
    icon: <ShopTwoTone className="imgbrand" twoToneColor="#000" />,
  },
  {
    icon: <BoxPlotTwoTone className="imgbrand" twoToneColor="#000" />,
  },
  {
    icon: <ApiTwoTone className="imgbrand" twoToneColor="#000" />,
  },
];

export const Contact = [
  {
    icon: <PhoneTwoTone className="img_cover" twoToneColor="#C40A2F" />,
    title: 'Contact Number',
    description: 'Contact Number',
  },
  {
    icon: <EnvironmentTwoTone className="img_cover" twoToneColor="#C40A2F" />,
    title: 'Office Address',
    description: 'Washington, D.C., USA',
  },
  {
    icon: <MailTwoTone className="img_cover" twoToneColor="#C40A2F" />,
    title: 'Mail Us',
    description: 'umi@gmail.com',
  },
];
export const Feature = [
  {
    description: '📦 Out of box, with built-in support for react, react-router, etc.',
  },
  {
    description:
      '🏈 Next.js like and full featured routing conventions, which also supports configured routing',
  },
  {
    description:
      '🎉 Complete plugin system, covering every lifecycle from source code to production',
  },
  {
    description:
      '🚀 High performance, with support for PWA, route-level code splitting, etc. via plugins',
  },
  {
    description:
      ' 💈 Support for static export, adapt to various environments, such as console app, mobile app, egg, Alipay wallet, etc',
  },
  {
    description: '🚄 Fast dev startup, support enable dll with config',
  },
  {
    description: '🐠 Compatible with IE9, based on umi-plugin-polyfills',
  },
  {
    description: '🍁 Support TypeScript, including d.ts definition and umi test',
  },
  {
    description:
      '🌴 Deep integration with dva, support duck directory, automatic loading of model, code splitting, etc',
  },
];
export const HomeFeature = [
  {
    icon: (
      <img
        alt="example"
        src="https://gw.alipayobjects.com/zos/basement_prod/a1c647aa-a410-4024-8414-c9837709cb43/k7787itw_w126_h114.png"
        className="img_cover_f"
      />
    ),
    title: 'Extensible',
    description:
      "Umi implements the complete lifecycle and makes it extensible, and Umi's internal functions are all plugins. Umi also support plugins and presets.",
  },
  {
    icon: (
      <img
        alt="example"
        src="https://gw.alipayobjects.com/zos/basement_prod/b54b48c7-087a-4984-b150-bcecb40920de/k7787z07_w114_h120.png"
        className="img_cover_f"
      />
    ),
    title: 'Out of the Box',
    description:
      'Umi has built-in routing, building, deployment, testing, and so on. It only requires one dependency to get started. Umi also provides an integrated preset for React with rich functionaries. ',
  },
  {
    icon: (
      <img
        alt="example"
        src="https://gw.alipayobjects.com/zos/basement_prod/67b771c5-4bdd-4384-80a4-978b85f91282/k7788ov2_w126_h126.png"
        className="img_cover_f"
      />
    ),
    title: 'Perfect Routing',
    description:
      'Supports both configuration routing and convention routing, while with functional completeness, such as dynamic routing, nested routing, permission routing, and so on. ',
  },
  {
    icon: (
      <img
        alt="example"
        src="https://gw.alipayobjects.com/zos/basement_prod/464cb990-6db8-4611-89af-7766e208b365/k77899wk_w108_h132.png"
        className="img_cover_f"
      />
    ),
    title: 'Enterprise',
    description:
      'It has been verified by 3000+ projects in Ant Financial and projects of Alibaba, Youku, Netease, 飞猪, KouBei and other companies.',
  },
  {
    icon: (
      <img
        alt="example"
        src="https://gw.alipayobjects.com/zos/basement_prod/201bea40-cf9d-4be2-a1d8-55bec136faf2/k7788a8s_w102_h120.png"
        className="img_cover_f"
      />
    ),
    title: 'Self Development',
    description:
      'It has been verified by 3000+ projects in Ant Financial and projects of Alibaba, Youku, Netease, 飞猪, KouBei and other companies.',
  },
];

export const SupportFeature = [
  {
    icon: <img alt="example" src={Support} className="img_cover_f" />,
    title: '24/7 Support',
    description: 'support 24 hours and 7 days a week',
  },
  {
    icon: <img alt="example" src={RequestDemo} className="img_cover_f" />,
    title: 'Request Demo',
    description: 'Experience a live customized demo',
  },
  {
    icon: <img alt="example" src={Inquiry} className="img_cover_f" />,
    title: 'Inquiry',
    description: 'How can we help you? Get instant answers.',
  },
  {
    icon: <img alt="example" src={Help} className="img_cover_f" />,
    title: 'Help Center',
    description: 'Learn what to do if youre having trouble',
  },
];
