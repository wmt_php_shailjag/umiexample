export default [
    {
      path: '/',
      component: '../layouts/index',
      title: 'UmiExample',
      routes: [
        {
          path: '/',
          component: './index',
          title: 'main page',
        },
        {
          path: '/home',
          component: './Home/home',
          title: 'Home page',
        },

        {
          path: '/contact',
          component: './Contact/contact',
          title: 'contact page',
        },
        {
          path: '/about',
          component: './About/about',
          title: 'about page',
        },
        {
          path: '/signin',
          component: './Signin/signin',
          title: 'Welcome back',
        },
        {
          path: '/signup',
          component: './Signup/signup',
          title: 'Welcome',
        },
        {
          path: '/404',
          component: './404/404',
          title: 'page not found',
        },
      ],
     
    },
    {
      component:'./404/404',
      title:'page not found'
  }
  ];
  